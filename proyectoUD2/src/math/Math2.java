package math;

public class Math2 {

   public static void main(String[] args) {
      int num = 5;
      for (int base = 0; base < 10; base++) {
         System.out.println("Logaritmo de 5 en base " + base + " = " + log(num, base));
      }
   }

   private static Double log(double num, int base) {
      return (Math.log10(num) / Math.log10(base));
   }

}