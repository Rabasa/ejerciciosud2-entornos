package math;

public class Math3 {

	  public static void main(String[] args) {
	     
	    System.out.print("Introduzca un n�mero real como base: ");
	    double base = Double.parseDouble(System.console().readLine());
	    
	    System.out.print("Introduzca un m�mero entero como exponente: ");
	    int exponenteFinal = Integer.parseInt(System.console().readLine());

	    double potencia;
	    int exponente;
	        
	    for (int i = 1; i <= exponenteFinal; i++) {
	        
	      potencia = 1;
	      exponente = i;
	      
	      for (int j = 0; j < exponente; j++) {
	        potencia *= base;
	      }
	      
	      System.out.println(base + "^" + i + " = " + potencia);
	    }

	  }
	}

	


